﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using Custom_Editor;
#endif

namespace Custom_Transformation
{    
    [Serializable]
    public class Transformation
    {
        public enum InputType
        {
            POINT,
            VECTOR
        }

        public enum Space
        {
            WORLD,
            LOCAL,
            COMBINE
        }

        [SerializeField] private List<Matrix4x4> m_saved_transformations;
        [SerializeField] private int m_modification_flag = 1; // 1 is TRS, 0 is Matrix, in line with read_write_option
        public Matrix4x4 LocalAxis = Matrix4x4.identity;
        public Matrix4x4 WorldAxis = Matrix4x4.identity;
        public Matrix4x4 WorldAxisDirty = Matrix4x4.identity;
        public Matrix4x4 LocalAxisDirty = Matrix4x4.identity;
        [HideInInspector] public Matrix4x4 CombinedAxis {get { return WorldAxis * LocalAxis; }}
        [HideInInspector] public Matrix4x4 CombinedAxisDirty {get { return WorldAxisDirty * LocalAxisDirty; }}        
        public Transformation()
        {
            m_saved_transformations = new List<Matrix4x4>();                                 
        }                
        
        #region AXIS
        public Matrix4x4 GetAxisMatrix(Space space)
        {
            var axis = Matrix4x4.identity;

            switch (space)
            {
                case Transformation.Space.WORLD:
                    if (m_modification_flag == 1) axis = WorldAxis;
                    if (m_modification_flag == 0) axis = WorldAxisDirty;            
                    break;
                case Transformation.Space.LOCAL:
                    if (m_modification_flag == 1) axis = LocalAxis;
                    if (m_modification_flag == 0) axis = LocalAxisDirty;
                    break;
                case Transformation.Space.COMBINE:
                    if (m_modification_flag == 1) axis = CombinedAxis;
                    if (m_modification_flag == 0) axis = CombinedAxisDirty;
                    break;
            }
            return axis;
        }

        public void SetAxisMatrix(Matrix4x4 matrix, Space space = Space.WORLD)
        {            
            switch (space)
            {
                case Space.WORLD:
                    if (m_modification_flag == 1) WorldAxis = matrix;;
                    if (m_modification_flag == 0) WorldAxisDirty = matrix;                                        
                    break;
                case Space.LOCAL:
                    if (m_modification_flag == 1) LocalAxis = matrix;;;
                    if (m_modification_flag == 0) LocalAxisDirty = matrix;                                                    
                    break;                
            }                        
        }        

        public void UseDirtyAxis(bool use_dirty)
        {
            m_modification_flag = use_dirty ? 0 : 1;
        }

        public Matrix4x4 TRSAxis (Vector3 t, Vector3 r, Vector3 s, Space space = Space.WORLD)
        {            
            return TRSAxis(Translate(t), Rotate(r), Scale(s), space);
        }
        
        public Matrix4x4 TRSAxis(Matrix4x4 m_t, Matrix4x4 m_r, Matrix4x4 m_s, Space space = Space.WORLD)
        {
            Matrix4x4 m = new Matrix4x4();
            switch (space)
            {
                case Space.WORLD:
                    WorldAxis = m_t * m_r * m_s;                    
                    m = WorldAxis;
                    break;
                case Space.LOCAL:
                    LocalAxis = m_t * m_r * m_s;
                    m = LocalAxis;
                    break;                    
            }                        
            return m;
        }        
        #endregion

        #region SAVED TRANSFORMS
        public List<Matrix4x4> GetSavedTransforms()
        {            
            return m_saved_transformations;
        }        

        public void SetSavedTransforms(List<Matrix4x4> list)
        {
            m_saved_transformations = list;
        }
        
        public void SaveTranslation(Vector3 translation)
        {
            m_saved_transformations.Add(Translate(translation));            
        }
        
        // Input: rotation in degrees
        public void SaveRotation(Vector3 rotation)
        {            
            m_saved_transformations.Add(Rotate(rotation));            
        }

        // Input: scale factor for each axis
        public void SaveScale(Vector3 scale)
        {                            
            m_saved_transformations.Add(Scale(scale));            
        }

        public void SaveMatrix(Matrix4x4 matrix)
        {                            
            m_saved_transformations.Add(matrix);            
        }                
        
        public void ClearSavedTransform()
        {
            m_saved_transformations.Clear();
        }       
        #endregion    

        #region STATIC
        public static Matrix4x4 TRS(Vector3 translate, Vector3 rotate, Vector3 scale)
        {
            return Translate(translate) * Rotate(rotate) * Scale(scale);
        }

        public static Matrix4x4 Scale(Vector3 scale)
        {
            Matrix4x4 t = new Matrix4x4();
            t.SetRow(0, new Vector4(scale.x, 0f, 0f, 0));
            t.SetRow(1, new Vector4(0f, scale.y, 0f, 0));
            t.SetRow(2, new Vector4(0f, 0f, scale.z, 0));
            t.SetRow(3, new Vector4(0f, 0f, 0f, 1f));
            
            return t;
        }

        public static Matrix4x4 Rotate(Vector3 rotation)
        {
            Matrix4x4 t = new Matrix4x4();
            // get rotation in radians
            var rotx = rotation.x * Mathf.Deg2Rad;
            var roty = rotation.y * Mathf.Deg2Rad;
            var rotz = rotation.z * Mathf.Deg2Rad;            
            // find trigo functions
            var sinX = Mathf.Sin(rotx);
            var cosX = Mathf.Cos(rotx);
            var sinY = Mathf.Sin(roty);
            var cosY = Mathf.Cos(roty);
            var sinZ = Mathf.Sin(rotz);
            var cosZ = Mathf.Cos(rotz);

            // combine matrix together in order rotx, roty, rotz
            // t.SetRow(0, new Vector4(cosY*cosZ, cosY*sinZ, -sinY, 0));
            // t.SetRow(1, new Vector4(-cosX*sinZ + sinX*sinY*cosZ, cosX*cosZ + sinX*sinY*sinZ, sinX*cosY, 0));
            // t.SetRow(2, new Vector4(sinX*sinZ + cosX*sinY*cosZ, -sinX*cosZ + cosX*sinY*sinZ, cosX*cosY, 0));
            // t.SetRow(3, new Vector4(0f, 0f, 0f, 1f));

			t.SetColumn(0, new Vector4(
				cosY * cosZ,
				cosX * sinZ + sinX * sinY * cosZ,
				sinX * sinZ - cosX * sinY * cosZ,
				0f
			));
			t.SetColumn(1, new Vector4(
				-cosY * sinZ,
				cosX * cosZ - sinX * sinY * sinZ,
				sinX * cosZ + cosX * sinY * sinZ,
				0f
			));
			t.SetColumn(2, new Vector4(
				sinY,
				-sinX * cosY,
				cosX * cosY,
				0f
			));
			t.SetColumn(3, new Vector4(0f, 0f, 0f, 1f));            
            return t;
        }

        public static Matrix4x4 Translate(Vector3 translation)
        {
            Matrix4x4 t = new Matrix4x4();
            t.SetRow(0, new Vector4(1, 0f, 0f, translation.x));
            t.SetRow(1, new Vector4(0f, 1, 0f, translation.y));
            t.SetRow(2, new Vector4(0f, 0f, 1, translation.z));
            t.SetRow(3, new Vector4(0f, 0f, 0f, 1f));

            return t;
        }        

        public static Vector3 DecomposeTranslate(Matrix4x4 m)
        {
            Vector3 v = Vector3.zero;
            v.x = m.m03;
            v.y = m.m13;
            v.z = m.m23;            

            return v;
        }

        public static Vector3 DecomposeRotation(Matrix4x4 m)
        {
            Vector3 v = Vector3.zero;


            return v;
        }

        public static Vector3 DecomposeScale(Matrix4x4 m)
        {
            Vector3 v = Vector3.zero;

            return v;
        }
        
        public static Vector3 Lerp(Vector3 point, Matrix4x4 initial, Matrix4x4 target, float dt)
        {            
            // get interpolated values between target and current transformation based on dt
            Matrix4x4 interpolated = new Matrix4x4();
                        
            return interpolated.MultiplyPoint(point);
        }        

        public static Vector3 Apply(Vector3 input, Matrix4x4 matrix, InputType type)
        {
            switch (type)
            {
                case InputType.POINT:
                    var pos = matrix.MultiplyPoint(input);
                    if (pos.x.Equals(float.NaN)) pos.x = input.x;
                    if (pos.x.Equals(float.PositiveInfinity)) pos.x = float.MaxValue;
                    if (pos.x.Equals(float.NegativeInfinity)) pos.x = float.MinValue;

                    if (pos.y.Equals(float.NaN)) pos.y = input.y;
                    if (pos.y.Equals(float.PositiveInfinity)) pos.y = float.MaxValue;
                    if (pos.y.Equals(float.NegativeInfinity)) pos.y = float.MinValue;

                    if (pos.z.Equals(float.NaN)) pos.z = input.z;
                    if (pos.z.Equals(float.PositiveInfinity)) pos.z = float.MaxValue;
                    if (pos.z.Equals(float.NegativeInfinity)) pos.z = float.MinValue;
                    
                    return pos;                    
                case InputType.VECTOR:
                    var vec = matrix.MultiplyPoint(input);
                    if (vec.x.Equals(float.NaN)) vec.x = input.x;
                    if (vec.x.Equals(float.PositiveInfinity)) vec.x = float.MaxValue;
                    if (vec.x.Equals(float.NegativeInfinity)) vec.x = float.MinValue;

                    if (vec.y.Equals(float.NaN)) vec.y = input.y;
                    if (vec.y.Equals(float.PositiveInfinity)) vec.y = float.MaxValue;
                    if (vec.y.Equals(float.NegativeInfinity)) vec.y = float.MinValue;

                    if (vec.z.Equals(float.NaN)) vec.z = input.z;
                    if (vec.z.Equals(float.PositiveInfinity)) vec.z = float.MaxValue;
                    if (vec.z.Equals(float.NegativeInfinity)) vec.z = float.MinValue;
                    
                    return vec;                    
            }            
            return input;
        }        
        #endregion
    }
    
    public class Mesh : MonoBehaviour
    {
        public Vector3 Size = new Vector3(2, 2, 2);
        public Transformation Transformations;        
        public GameObject Prefab;

        [HideInInspector] public Transform[] V;

        private bool m_is_instantiated = false;        
        // Start is called before the first frame update
        void Awake() 
        {
            Initialize_Awake();    
        }
        
        void Start()
        {
            Instantiate();
        }

        Transform create (int x, int y, int z) {
            #if UNITY_EDITOR            
            GameObject vert = null;
            if (!EditorApplication.isPlaying)
            {
                if (Prefab != null)
                {
                    vert = Instantiation_Editor.Instantiate(Prefab);
                    Instantiation_Editor.SetRoot(vert, gameObject);
                }                                                    
                else
                    Debug.LogError("Prefab is empty!");
            }                            
            else
            {
                vert = Instantiate(Prefab);
                vert.transform.SetParent(transform);
            }                
            #else
            vert = Instantiate(Prefab);
            vert.transform.SetParent(transform);
            #endif

            vert = modify(vert, x, y, z);        
            return vert.transform;
        }

        GameObject modify(GameObject point, int x, int y, int z)
        {
            point.transform.localPosition = GetLocalPosition(x, y, z);
            // Color pointColor = new Color(
            //     (float)x / Size.x,
            //     (float)y / Size.y,
            //     (float)z / Size.z
            // );
            Color pointColor = Color.black;                

            #if UNITY_EDITOR
            if (!EditorApplication.isPlaying)
            {
                var tempMaterial = new Material(point.GetComponent<MeshRenderer>().sharedMaterial);
                tempMaterial.color = pointColor;
                point.GetComponent<MeshRenderer>().sharedMaterial = tempMaterial;
            }
            else
            {
                point.GetComponent<MeshRenderer>().material.color = pointColor;
            }
            #else
            point.GetComponent<MeshRenderer>().material.color = pointColor;
            #endif
            return point;
        }

        public void Initialize_Awake()
        {
            Destroy();
            int grid_size = (int)Size.x* (int)Size.y* (int)Size.z;        
            if (grid_size <= 0)
                Debug.LogWarning("Mesh resolution is 0");
            else
            {
                V = new Transform[grid_size];                
            }                
        }

        public void Instantiate()
        {
            if (m_is_instantiated)
            {
                Debug.LogWarning("Mesh already exists, please create another instance of Mesh");
                return;
            }

            for (int i = 0, z = 0; z < (int)Size.z; z++) {
                for (int y = 0; y < (int)Size.y; y++) {
                    for (int x = 0; x < (int)Size.x; x++, i++) {                        
                        if (i < V.Length)
                            V[i] = create(x, y, z);
                    }
                }
            }
            m_is_instantiated = true;
        }

        public void Destroy()
        {
            if (V != null && V.Length > 0)
            {
                var i = V.Length - 1;
                while (i >= 0)
                {
                    if (V[i] == null) { i--; continue; }
                    #if UNITY_EDITOR
                    if (!EditorApplication.isPlaying)
                    {
                        DestroyImmediate(V[i].gameObject);
                    }
                    else
                    {
                        Destroy(V[i].gameObject);    
                    }                    
                    #else
                    Destroy(V[i].gameObject);
                    #endif
                    i--;
                }
                int grid_size = (int)Size.x* (int)Size.y* (int)Size.z;
                V = new Transform[grid_size];
                m_is_instantiated = false;
            } 
        }

        public Vector3 GetLocalPosition (int x, int y, int z) {
            float xx = (float) x - ((int) Size.x - 1) * 0.5f;
            float yy = (float) y - ((int) Size.y - 1) * 0.5f;
            float zz = (float) z - ((int) Size.z - 1) * 0.5f;
            return new Vector3(xx, yy, zz);
        }

        public Vector3 GetLocalPosition(Vector3 position)
        {
            return GetLocalPosition((int) position.x, (int) position.y, (int) position.z);
        }

        public void ApplySavedTransform()
        {
            var savedTransforms = Transformations.GetSavedTransforms();
            if (savedTransforms.Count <= 0 )
            {
                Debug.LogWarning("No transformation found: " + savedTransforms.Count);                
                return;
            }
            if (V == null || V.Length <= 0)
            {
                return;
            }            

            for (int i = 0, z = 0; z < (int)Size.z; z++) {
                for (int y = 0; y < (int)Size.y; y++) {
                    for (int x = 0; x < (int)Size.x; x++, i++) {
                        if (V[i] == null) continue;
                        foreach (var m in savedTransforms)
                        {                
                            V[i].localPosition = Transformation.Apply(new Vector3(x, y, z), m, Transformation.InputType.POINT);
                        }                         
                    }
                }
            }
        }

        public void ApplySavedTransform(int index)
        {
            var savedTransforms = Transformations.GetSavedTransforms();

            if (savedTransforms.Count <= 0 || index >= savedTransforms.Count)
            {
                Debug.LogWarning("No transformation found: " + savedTransforms.Count);                 
                return;
            }
            if (V == null || V.Length <= 0)
            {
                return;
            }

            // applies selected transformation to all vertices
            for (int i = 0, z = 0; z < (int)Size.z; z++) {
                for (int y = 0; y < (int)Size.y; y++) {
                    for (int x = 0; x < (int)Size.x; x++, i++) {
                        if (V[i] == null) continue;
                        V[i].localPosition = Transformation.Apply(new Vector3(x, y, z), savedTransforms[index], Transformation.InputType.POINT);                        
                    }
                }
            }
        }
        
        public void ApplySavedTransform(int fromIndex, int toIndex)
        {
            var savedTransforms = Transformations.GetSavedTransforms();

            if (fromIndex < 0 || toIndex >= savedTransforms.Count)
            {
                Debug.LogWarning("No transformation found: " + savedTransforms.Count);                 
                return;
            }
            if (V == null || V.Length <= 0)
            {
                return;
            }
            if (fromIndex == toIndex)
            {
                ApplySavedTransform(fromIndex);
                return;
            }

            // applies selected transformation to all vertices
            for (int i = 0, z = 0; z < (int)Size.z; z++) {
                for (int y = 0; y < (int)Size.y; y++) {
                    for (int x = 0; x < (int)Size.x; x++, i++) {
                        if (V[i] == null) continue;
                        for (int s=fromIndex; s <= toIndex; s++)
                        {
                            V[i].localPosition = Transformation.Apply(new Vector3(x, y, z), savedTransforms[i], Transformation.InputType.POINT);                                            
                        }                         
                    }
                }
            }
        }
        
        public void ResetSavedTransform()
        {
            // applies selected transformation to all vertices
            for (int i = 0, z = 0; z < (int)Size.z; z++) {
                for (int y = 0; y < (int)Size.y; y++) {
                    for (int x = 0; x < (int)Size.x; x++, i++) {
                        if (V[i] == null) continue;
                        V[i].localPosition = Transformation.Apply(new Vector3(x, y, z), Matrix4x4.identity, Transformation.InputType.POINT);
                    }
                }
            }
        }

        public void ApplyAxis(Transformation.Space space)
        {            
            if (V == null || V.Length <= 0)
            {
                return;
            }

            for (int i = 0, z = 0; z < (int)Size.z; z++) {
                for (int y = 0; y < (int)Size.y; y++) {
                    for (int x = 0; x < (int)Size.x; x++, i++) {
                        if (V[i] == null) continue;                        
                        V[i].localPosition = Transformation.Apply(new Vector3(x, y, z), Transformations.GetAxisMatrix(space), Transformation.InputType.POINT);                        
                    }
                }
            }
        }        

        public void ResetAxis(Transformation.Space space)
        {
            if (V == null || V.Length <= 0)
            {
                return;
            }            

            Transformations.SetAxisMatrix(Matrix4x4.identity, space);                                    

            for (int i = 0, z = 0; z < (int)Size.z; z++) {
                for (int y = 0; y < (int)Size.y; y++) {
                    for (int x = 0; x < (int)Size.x; x++, i++) {
                        if (V[i] == null) continue;
                        V[i].localPosition = Transformation.Apply(new Vector3(x, y, z), Transformations.GetAxisMatrix(space), Transformation.InputType.POINT);                        
                    }
                }
            }            
        }        
    }
}
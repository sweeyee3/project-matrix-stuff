﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Custom_Editor;

namespace Custom_Transformation
{
    [CustomEditor(typeof(Mesh))]
    public class Mesh_Editor : Editor
    {
        readonly Color ROOT_TRANSFORMATION_COLOR = Color.green;
        readonly Color COMBINED_TRANSFORMATION_COLOR = Color.red;
        readonly Color CHILD_TRANSFORMATION_COLOR = Color.yellow;
        readonly Color REFERENCE_TRANSFORMATION_COLOR = Color.white;
        readonly string COMBINED_ORDER = "Order of transform is important. By default, script uses this convention:\nCHILD(1st) -> ROOT(2nd)";

        SerializedProperty m_size;
        SerializedProperty m_transformation;
        SerializedProperty m_modification_flag; // 1 is modify  TRS, 0 is modify Matrix, in line with read_write_option
        SerializedProperty m_saved_transform;                
        Mesh m_target;
        string[]  m_excluded_properties;
        
        Matrix4x4 m_world_axis;
        Vector3 m_world_axis_pos;
        Vector3 m_world_axis_euler_angles;
        Vector3 m_world_axis_scale;
        bool m_display_world_axis;
        bool m_display_world_trs = false;
        bool m_display_world_matrix = false;        

        Matrix4x4 m_local_axis;
        Vector3 m_local_axis_pos;
        Vector3 m_local_axis_euler_angles;
        Vector3 m_local_axis_scale;
        bool m_display_local_axis;
        bool m_display_local_trs = false;
        bool m_display_local_matrix = false;                

        Matrix4x4 m_combined_axis;
        bool m_display_combined_matrix = true;
        string m_combined_transform_order;

        Matrix4x4 m_reference_axis = Matrix4x4.identity;
        bool m_display_reference_axis;

        Vector3 m_world_axis_origin = new Vector3(0, 0, 0);
        Vector3 m_world_axis_x = new Vector3(1, 0, 0);
        Vector3 m_world_axis_y = new Vector3(0, 1, 0);
        Vector3 m_world_axis_z = new Vector3(0, 0, 1);

        Vector3 m_local_axis_origin = new Vector3(0, 0, 0);
        Vector3 m_local_axis_x = new Vector3(1, 0, 0);
        Vector3 m_local_axis_y = new Vector3(0, 1, 0);
        Vector3 m_local_axis_z = new Vector3(0, 0, 1);

        Vector3 m_child_origin = new Vector3(0, 0, 0);
        Vector3 m_child_x = new Vector3(1, 0, 0);
        Vector3 m_child_y = new Vector3(0, 1, 0);
        Vector3 m_child_z = new Vector3(0, 0, 1);

        Vector3 m_refernce_origin = new Vector3(0, 0, 0);
        Vector3 m_reference_x = new Vector3(1, 0, 0);
        Vector3 m_reference_y = new Vector3(0, 1, 0);
        Vector3 m_reference_z = new Vector3(0, 0, 1);

        List<Matrix4x4> m_saved;
        uint m_saved_display; // stored using bitwise flags.

        GameObject m_local_camera;
        bool m_is_apply_continuous_axis;        
        private void OnEnable() 
        {
            if (serializedObject == null) return;
            
            m_size = serializedObject?.FindProperty("Size");
            m_transformation = serializedObject?.FindProperty("Transformations");            
            m_saved_transform = m_transformation?.FindPropertyRelative("m_saved_transformations");            
            
            m_target = (Mesh) target;
            m_excluded_properties = new string[4] { "Size", "LocalAxis", "WorldAxis", "Transformations" };
            
            m_world_axis_pos = Vector3.zero;
            m_world_axis_euler_angles = Vector3.zero;
            m_world_axis_scale = Vector3.one;
    
            m_local_axis_pos = Vector3.zero;
            m_local_axis_euler_angles = Vector3.zero;
            m_local_axis_scale = Vector3.one;                    
                        
            m_world_axis_origin = new Vector3(0, 0, 0);
            m_world_axis_x = new Vector3(1, 0, 0);
            m_world_axis_y = new Vector3(0, 1, 0);
            m_world_axis_z = new Vector3(0, 0, 1);

            m_local_axis_origin = new Vector3(0, 0, 0);
            m_local_axis_x = new Vector3(1, 0, 0);
            m_local_axis_y = new Vector3(0, 1, 0);
            m_local_axis_z = new Vector3(0, 0, 1);

            m_child_origin = new Vector3(0, 0, 0);
            m_child_x = new Vector3(1, 0, 0);
            m_child_y = new Vector3(0, 1, 0);
            m_child_z = new Vector3(0, 0, 1);

            m_saved = m_target.Transformations.GetSavedTransforms();
            m_saved_display = 0;                        

            if (m_local_camera == null)
            {
                m_local_camera = Instantiation_Editor.Instantiate("Assets/Scenes/Prefab/camera.prefab");
                m_local_camera.tag = "Untagged";
                m_local_camera.GetComponent<Camera>().targetDisplay = 100;
                Instantiation_Editor.SetRoot(m_local_camera, m_target.gameObject);
            }            

            m_is_apply_continuous_axis = true;
            m_modification_flag = m_transformation.FindPropertyRelative("m_modification_flag");

            if (m_target.V == null) m_target?.Initialize_Awake();            
        }

        private void OnDisable() 
        {            
            if (m_local_camera != null)
                DestroyImmediate(m_local_camera);
            else
            {
                Debug.LogWarning("No camera to destroy");
            }
        }

        private void OnSceneGUI() 
        {
            // draw world transform
            var color = Handles.color;

            Handles.color = ROOT_TRANSFORMATION_COLOR;
            var world_axis = m_modification_flag.intValue == 0 ? m_target.Transformations.WorldAxisDirty : m_target.Transformations.WorldAxis;
            Handles.DrawLine(world_axis.MultiplyPoint(m_world_axis_origin), world_axis.MultiplyPoint(m_world_axis_x)); // x-axis
            Handles.DrawLine(world_axis.MultiplyPoint(m_world_axis_origin), world_axis.MultiplyPoint(m_world_axis_y)); // y-axis
            Handles.DrawLine(world_axis.MultiplyPoint(m_world_axis_origin), world_axis.MultiplyPoint(m_world_axis_z)); // z-axis

            // draw combined transform            
            Handles.color = COMBINED_TRANSFORMATION_COLOR;

            var combined_axis = m_modification_flag.intValue == 0 ? m_target.Transformations.CombinedAxisDirty : m_target.Transformations.CombinedAxis;
            Handles.DrawLine(combined_axis.MultiplyPoint(m_local_axis_origin), combined_axis.MultiplyPoint(m_local_axis_x)); // x-axis
            Handles.DrawLine(combined_axis.MultiplyPoint(m_local_axis_origin), combined_axis.MultiplyPoint(m_local_axis_y)); // y-axis
            Handles.DrawLine(combined_axis.MultiplyPoint(m_local_axis_origin), combined_axis.MultiplyPoint(m_local_axis_z)); // z-axis

            // draw child transform            
            Handles.color = CHILD_TRANSFORMATION_COLOR;

            var child_axis = m_modification_flag.intValue == 0 ? m_target.Transformations.LocalAxisDirty : m_target.Transformations.LocalAxis;
            Handles.DrawLine(child_axis.MultiplyPoint(m_child_origin), child_axis.MultiplyPoint(m_child_x)); // x-axis
            Handles.DrawLine(child_axis.MultiplyPoint(m_child_origin), child_axis.MultiplyPoint(m_child_y)); // y-axis
            Handles.DrawLine(child_axis.MultiplyPoint(m_child_origin), child_axis.MultiplyPoint(m_child_z)); // z-axis

            // draw child transform            
            Handles.color = REFERENCE_TRANSFORMATION_COLOR;

            var reference_axis = Matrix4x4.identity;
            Handles.DrawLine(reference_axis.MultiplyPoint(m_refernce_origin), reference_axis.MultiplyPoint(m_reference_x)); // x-axis
            Handles.DrawLine(reference_axis.MultiplyPoint(m_refernce_origin), reference_axis.MultiplyPoint(m_reference_y)); // y-axis
            Handles.DrawLine(reference_axis.MultiplyPoint(m_refernce_origin), reference_axis.MultiplyPoint(m_reference_z)); // z-axis

            Handles.color = color;            
        }                              

        public override void OnInspectorGUI()
        {
            DrawPropertiesExcluding(serializedObject, m_excluded_properties);            

            #region INSTANTIATION
            // draw resolution of grid
            EditorGUILayout.LabelField("Instantiation options", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(m_size, new GUIContent("Resolution"));                       
            if (EditorGUI.EndChangeCheck())
            {                
                m_target?.Initialize_Awake();
            }

            // Instantiate
            if (GUILayout.Button("Create"))
            {                
                m_target?.Instantiate();
            }

            // destroys
            if (GUILayout.Button("Destroy"))
            {
                m_target?.Destroy();      
            }                                  

            // should apply continuous?
            m_is_apply_continuous_axis = EditorGUILayout.Toggle("Continuous", m_is_apply_continuous_axis);
            #endregion

            GUILayout.Space(20);
            
            #region Reference Axis
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(m_display_reference_axis ? "v" : ">", GUILayout.Width(25)))
            {
                m_display_reference_axis = !m_display_reference_axis;                
            }
            var reference_axis_color = EditorStyles.largeLabel.normal.textColor;
            EditorStyles.largeLabel.normal.textColor = REFERENCE_TRANSFORMATION_COLOR;
            EditorGUILayout.LabelField("Reference Transformation", EditorStyles.largeLabel);
            EditorStyles.largeLabel.normal.textColor = reference_axis_color;            
            EditorGUILayout.EndHorizontal();

            if (m_display_reference_axis)
                displayMatrix4x4("ReferenceAxis", 1, ref m_reference_axis);
            #endregion

            GUILayout.Space(20);

            #region Local Axis
            EditorGUILayout.BeginHorizontal();            
            if (GUILayout.Button(m_display_local_axis ? "v" : ">", GUILayout.Width(25)))
            {
                m_display_local_axis = !m_display_local_axis;                
            }
            var child_axis_color = EditorStyles.largeLabel.normal.textColor;
            EditorStyles.largeLabel.normal.textColor = CHILD_TRANSFORMATION_COLOR;
            EditorGUILayout.LabelField("Child Transformation", EditorStyles.largeLabel);
            EditorStyles.largeLabel.normal.textColor = child_axis_color;            

            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Reset", GUILayout.Width(45)))
            {
                m_target?.ResetAxis(Transformation.Space.LOCAL);
                m_local_axis_pos = Vector3.zero;
                m_local_axis_euler_angles = Vector3.zero;
                m_local_axis_scale = Vector3.one;                
            }                                    
            EditorGUILayout.EndHorizontal();            
            
            if (m_display_local_axis)
            {
                // displau local axis TRS options
                displayTRSOptions(ref m_local_axis_pos, ref m_local_axis_euler_angles, ref m_local_axis_scale, Custom_Transformation.Transformation.Space.LOCAL);
                
                GUILayout.Space(10);

                // grab matrix 4x4 and display in their own column 
                displayMatrix4x4("LocalAxis", m_modification_flag.intValue, ref m_local_axis);            
            }            
            #endregion

            GUILayout.Space(20);

            #region World Axis
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(m_display_world_axis ? "v" : ">", GUILayout.Width(25)))
            {
                m_display_world_axis = !m_display_world_axis;                
            }

            var world_axis_color = EditorStyles.largeLabel.normal.textColor;
            EditorStyles.largeLabel.normal.textColor = ROOT_TRANSFORMATION_COLOR;
            EditorGUILayout.LabelField("Root Transformation", EditorStyles.largeLabel);
            EditorStyles.largeLabel.normal.textColor = world_axis_color;

            GUILayout.FlexibleSpace();            
            if (GUILayout.Button("Reset", GUILayout.Width(45)))
            {
                m_target?.ResetAxis(Transformation.Space.WORLD);
                m_world_axis_pos = Vector3.zero;
                m_world_axis_euler_angles = Vector3.zero;
                m_world_axis_scale = Vector3.one;                
            }            
            EditorGUILayout.EndHorizontal();                                    

            if (m_display_world_axis)
            {                                
                // display world axis TRS options
                displayTRSOptions(ref m_world_axis_pos, ref m_world_axis_euler_angles, ref m_world_axis_scale, Custom_Transformation.Transformation.Space.WORLD);

                GUILayout.Space(10);

                // grab matrix 4x4 and display in their own column 
                displayMatrix4x4("WorldAxis", m_modification_flag.intValue, ref m_world_axis);                                
            }            
            #endregion

            GUILayout.Space(20);

            #region Combined Transform
            EditorGUILayout.BeginHorizontal();            
            if (GUILayout.Button(m_display_combined_matrix ? "v" : ">", GUILayout.Width(25)))
            {
                m_display_combined_matrix = !m_display_combined_matrix;                
            }
            var local_axis_color = EditorStyles.largeLabel.normal.textColor;
            EditorStyles.largeLabel.normal.textColor = COMBINED_TRANSFORMATION_COLOR;            
            EditorGUILayout.LabelField("Combined Transformation", EditorStyles.largeLabel);
            EditorStyles.largeLabel.normal.textColor = local_axis_color;            
            EditorGUILayout.EndHorizontal();
            
            if (m_display_combined_matrix)
            {
                EditorGUILayout.Space();
                EditorGUILayout.SelectableLabel(COMBINED_ORDER, EditorStyles.largeLabel);
                EditorGUILayout.Space();
                displayMatrix4x4("CombinedAxis", 1, ref m_combined_axis);
                if (GUILayout.Button("Save Combined Transformation"))
                {
                    m_target.Transformations.SaveMatrix(m_combined_axis);
                    serializedObject.ApplyModifiedProperties();
                }
            }            

            if (m_is_apply_continuous_axis)
            {
                m_target.ApplyAxis(Transformation.Space.COMBINE);
            }                        
            #endregion

            GUILayout.Space(20);

            #region Camera
            // display camera viewport
            if (m_local_camera?.GetComponent<Camera>() != null)
            {                
                var width = EditorGUIUtility.currentViewWidth > 300 ? 250 : EditorGUIUtility.currentViewWidth - 50;                
                var rect = EditorGUILayout.GetControlRect(GUILayout.Height(200), GUILayout.Width(width));
                var offset = EditorGUIUtility.currentViewWidth - width < 0 ? 0 : (EditorGUIUtility.currentViewWidth - width) / 2;                                
                
                rect.position = new Vector2(offset, rect.position.y);
                Handles.DrawCamera(rect, m_local_camera.GetComponent<Camera>());
            }                                            
            #endregion                                                

            GUILayout.Space(20);
            
            #region Saved Transform
            if (m_saved_transform.arraySize > 0)
            {
                EditorGUILayout.BeginHorizontal();                                                               
                EditorGUILayout.LabelField("Saved Transforms", EditorStyles.largeLabel);
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Reset", GUILayout.Width(45)))
                {
                    m_target?.ResetSavedTransform();                    
                }
                // delete button
                if (GUILayout.Button("X", GUILayout.Width(25)))
                {
                    m_target.Transformations.ClearSavedTransform();
                    serializedObject.ApplyModifiedProperties();
                }                
                EditorGUILayout.EndHorizontal();
                
                // display saved transform                
                m_saved = m_target.Transformations.GetSavedTransforms();
                
                var removeIndex = -1;
                for (var i=0; i<m_saved.Count; i++)
                {
                    uint index_bool = (uint) 1 << i;                    

                    EditorGUILayout.BeginHorizontal();
                    // display button
                    if (GUILayout.Button( (m_saved_display & index_bool) == 0 ? ">" : "v", GUILayout.Width(25)))
                    {
                        m_saved_display ^= index_bool;
                    }                    
                    EditorGUILayout.LabelField("matrix " + i);
                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button("Apply"))
                    {
                        // trigger function to apply trasnformation
                        m_is_apply_continuous_axis = false;
                        m_target?.ApplySavedTransform(i);
                    }
                    // delete button
                    if (GUILayout.Button("X", GUILayout.Width(25)))
                    {
                        removeIndex = i;
                    }
                    EditorGUILayout.EndHorizontal();

                    if ((m_saved_display & index_bool) != 0 && removeIndex < 0)
                    {
                        m_saved[i] = displayMatrix4x4(m_saved[i], 0);
                    }                                        
                }                
                if (removeIndex >= 0) m_saved.RemoveAt(removeIndex);
                m_target.Transformations.SetSavedTransforms(m_saved);
                serializedObject.ApplyModifiedProperties();                                                                                
            }
            #endregion
        }

        private void displayTRSOptions(ref Vector3 axis_pos, ref Vector3 axis_euler, ref Vector3 axis_scale, Custom_Transformation.Transformation.Space space)
        {
            bool unlocked = false;
            // insert vector3 rotation options
            EditorGUILayout.BeginHorizontal();
            var displayOption = space == Custom_Transformation.Transformation.Space.LOCAL ? m_display_local_trs : m_display_world_trs;

            if (GUILayout.Button(displayOption ? "v" : ">", GUILayout.Width(25)))
            {                
                m_display_local_trs = !displayOption;
                m_display_world_trs = !displayOption;
            }
            EditorGUILayout.LabelField("TRS options", EditorStyles.boldLabel);
            // add unlock / lock button here
            if (m_modification_flag.intValue == 0)
            {
                if (GUILayout.Button("Unlock", GUILayout.Width(50)))
                {
                    m_modification_flag.intValue = 1;
                    m_display_local_matrix = false;
                    m_display_world_matrix = false;
                    m_display_local_trs = true;
                    m_display_world_trs = true;                    
                    serializedObject.ApplyModifiedProperties();
                    unlocked = true;                    
                }
            }             
            EditorGUILayout.EndHorizontal();         
            
            EditorGUI.BeginChangeCheck();

            if (m_display_local_trs || m_display_world_trs)
            {
                // translate
                displayTRSVector3(ref axis_pos, "T", m_modification_flag.intValue);

                // rotate
                displayTRSVector3(ref axis_euler, "R", m_modification_flag.intValue); 

                // scale
                displayTRSVector3(ref axis_scale, "S", m_modification_flag.intValue);
            }                            

             // apply TRS combine if applicable
            if ((EditorGUI.EndChangeCheck() && m_modification_flag.intValue == 1) || unlocked)
            {                
                m_target.Transformations.TRSAxis(axis_pos, axis_euler, axis_scale, space);                
            }                        

            serializedObject.ApplyModifiedProperties();            
        }        

        private void displayMatrix4x4(string propertyName, int read_write_option, ref Matrix4x4 matrix)
        {
            // hide and display matrix
            if (propertyName != "CombinedAxis" && propertyName != "ReferenceAxis")
            {
                EditorGUILayout.BeginHorizontal();
                var displayOption = propertyName == "LocalAxis" ? m_display_local_matrix : m_display_world_matrix;

                if (GUILayout.Button(displayOption ? "v" : ">", GUILayout.Width(25)))
                {                
                    m_display_local_matrix = !displayOption;
                    m_display_world_matrix = !displayOption;
                }
                EditorGUILayout.LabelField("Matrix", EditorStyles.boldLabel);
                // add unlock / lock button here
                if (m_modification_flag.intValue == 1)
                {
                    if (GUILayout.Button("Unlock", GUILayout.Width(50)))
                    {
                        m_modification_flag.intValue = 0;
                        m_display_local_trs = false;
                        m_display_world_trs = false;
                        m_display_local_matrix = true;
                        m_display_world_matrix = true;
                        serializedObject.ApplyModifiedProperties();
                    }
                }  
                EditorGUILayout.EndHorizontal();          
            }            

            if (m_target != null)
            {
                // swap between correct axes depending on name and also, if it is a TRS or dirty matrix (modified)
                if (propertyName == "LocalAxis")
                    matrix = m_modification_flag.intValue == 0 ? m_target.Transformations.LocalAxisDirty : m_target.Transformations.LocalAxis;
                if (propertyName == "WorldAxis")
                    matrix = m_modification_flag.intValue == 0 ? m_target.Transformations.WorldAxisDirty : m_target.Transformations.WorldAxis;
                if (propertyName == "CombinedAxis")
                    matrix = m_modification_flag.intValue == 0 ? m_target.Transformations.CombinedAxisDirty : m_target.Transformations.CombinedAxis;
                if (propertyName == "ReferenceAxis")
                    matrix = m_reference_axis;
            }
            
            if (m_display_local_matrix || m_display_world_matrix || propertyName == "CombinedAxis" || propertyName == "ReferenceAxis")
                matrix = displayMatrix4x4(matrix, read_write_option);

            if (m_target != null)
            {
                if (propertyName == "LocalAxis")
                {
                    if (read_write_option == 0) m_target.Transformations.LocalAxisDirty = matrix; // apply new matrix to axis for unity to serialize because serializing matrix4x4 is not known currently.
                    if (read_write_option == 1) m_target.Transformations.LocalAxis = matrix; // apply new matrix to axis for unity to serialize because serializing matrix4x4 is not known currently.
                    // TODO: decompose matrix to TRS
                }                 
                if (propertyName == "WorldAxis")
                {
                    if (read_write_option == 0) m_target.Transformations.WorldAxisDirty = matrix; // apply new matrix to axis for unity to serialize because serializing matrix4x4 is not known currently.
                    if (read_write_option == 1) m_target.Transformations.WorldAxis = matrix; // apply new matrix to axis for unity to serialize because serializing matrix4x4 is not known currently.
                    // TODO: decompose matrix to TRS
                }                                    
            }                            
            
            serializedObject.ApplyModifiedProperties();            
        }

        private Vector3 displayTRSVector3(ref Vector3 axis, string name, int read_write_option)
        {
            float lab_width = EditorGUIUtility.labelWidth;
            float fld_width = EditorGUIUtility.fieldWidth;

            EditorGUIUtility.labelWidth = 30;
            EditorGUIUtility.fieldWidth = 50;

            EditorGUILayout.BeginHorizontal();
            if (read_write_option == 1)
            {
                axis = EditorGUILayout.Vector3Field(name, axis);
                if (GUILayout.Button("Reset", GUILayout.Width(45)))
                {
                    axis = Vector3.one;                                
                }                       
            }
            else if (read_write_option == 0)
            {
                EditorGUILayout.LabelField(name);
                GUILayout.FlexibleSpace();
                EditorGUILayout.LabelField(axis.x.ToString());
                EditorGUILayout.LabelField(axis.y.ToString());
                EditorGUILayout.LabelField(axis.z.ToString());
            }            
            EditorGUILayout.EndHorizontal();

            EditorGUIUtility.labelWidth = lab_width;
            EditorGUIUtility.fieldWidth = fld_width;

            return axis;
        }

        private Matrix4x4 displayMatrix4x4(Matrix4x4 matrix, int read_write_option)
        {
            float lab_width = EditorGUIUtility.labelWidth;
            float fld_width = EditorGUIUtility.fieldWidth;

            EditorGUIUtility.labelWidth = 30;
            EditorGUIUtility.fieldWidth = 50;

            for(var i=0; i<4; i++)
            {
                EditorGUILayout.BeginHorizontal();
                for(var j=0; j<4; j++)
                {                                        
                    string header = "m" + i + j;
                    if (read_write_option == 0)             
                        matrix[i, j] = EditorGUILayout.FloatField(header, matrix[i, j]);
                    else if (read_write_option == 1)
                        EditorGUILayout.LabelField(matrix[i, j].ToString());
                }
                EditorGUILayout.EndHorizontal();
            }

            EditorGUIUtility.labelWidth = lab_width;
            EditorGUIUtility.fieldWidth = fld_width;
            return matrix;
        }
        
        private SerializedProperty[,] deserializeMatrix4x4(string name)
        {
            SerializedProperty[,] fields = new SerializedProperty[4, 4];
            for (var i=0; i<fields.GetLength(0); i++)
            {
                for (var j=0; j<fields.GetLength(1); j++)
                {
                    fields[i, j] = m_transformation.FindPropertyRelative(name).FindPropertyRelative("e" + i + j);
                }
            }

            return fields;
        }                                            
    }
}
#endif
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public interface ISimpleStateMachine<T>
{
    T CurrentState { get; set; }
    T PreviousState { get; set; }
    void ChangeState(T newState, bool changeStateDirty = false);
    void OnStateEnter(T currentState);
    void OnStateExit(T previousState);
    void OnStateUpdate();
    void ReturnToPreviousState();
    bool IsInState(T state);
}

public interface ISimpleState<T>
{
    void Enter(ISimpleStateMachine<ISimpleState<T>> stateMachine);
    void Exit(ISimpleStateMachine<ISimpleState<T>> stateMachine);
    void Update(ISimpleStateMachine<ISimpleState<T>> stateMachine);
}

public class TestClassStateMachine : ISimpleStateMachine<ISimpleState<TestClassStateMachine.TestParentState>>
{
    public class TestParentState : ISimpleState<TestParentState>
    {
        public void Enter(ISimpleStateMachine<ISimpleState<TestParentState>> stateMachine)
        {

        }

        public void Exit(ISimpleStateMachine<ISimpleState<TestParentState>> stateMachine)
        {

        }

        public void Update(ISimpleStateMachine<ISimpleState<TestParentState>> statmMachine)
        {

        }
    }
    public class TestChildStateOne : TestParentState
    {

    }

    public ISimpleState<TestParentState> CurrentState { get; set; }
    public ISimpleState<TestParentState> PreviousState { get; set; }

    public void ChangeState(ISimpleState<TestParentState> newState, bool changeStateDirty = false)
    {
        var isChangeState = PreviousState != CurrentState || changeStateDirty;

        if (isChangeState)
        {
            PreviousState = CurrentState;
            OnStateExit(PreviousState);
            CurrentState = newState;
            OnStateEnter(CurrentState);
        }
    }

    public bool IsInState(ISimpleState<TestParentState> state)
    {
        return CurrentState == state;
    }

    public void OnStateEnter(ISimpleState<TestParentState> currentState)
    {
        currentState.Enter(this);
    }

    public void OnStateExit(ISimpleState<TestParentState> previousState)
    {
        previousState.Exit(this);
    }

    public void OnStateUpdate()
    {
        CurrentState.Update(this);
    }

    public void ReturnToPreviousState()
    {
        throw new System.NotImplementedException();
    }
}

public class TestEnumStateMachine : ISimpleStateMachine<TestEnumStateMachine.TestEnum>, ISubject<IObserver<TestEnumStateMachine.TestEnum>>
{
    public enum TestEnum
    {
        TEST1,
        TEST2,
        TEST3
    }

    public TestEnum CurrentState { get; set; }
    public TestEnum PreviousState { get; set; }
    public List<IObserver<TestEnum>> Observers { get; set; }

    public void AddObserver(IObserver<TestEnum> observer)
    {
        if (!Observers.Contains(observer))
            Observers.Add(observer);
    }

    public void RemoveObserver(IObserver<TestEnum> observer)
    {
        if (Observers.Contains(observer))
            Observers.Remove(observer);
    }

    public void ChangeState(TestEnum newState, bool changeStateDirty = false)
    {
        var isChangeState = PreviousState != CurrentState || changeStateDirty;

        if (isChangeState)
        {
            PreviousState = CurrentState;
            OnStateExit(PreviousState);
            CurrentState = newState;
            OnStateEnter(CurrentState);
        }
    }

    public bool IsInState(TestEnum state)
    {
        return CurrentState == state;
    }

    public void OnStateEnter(TestEnum currentState)
    {
        foreach (var observer in Observers)
        {
            observer.OnNotifyEnter(currentState);
        }
    }

    public void OnStateExit(TestEnum previousState)
    {
        foreach (var observer in Observers)
        {
            observer.OnNotifyExit(previousState);
        }
    }

    public void OnStateUpdate()
    {
        foreach (var observer in Observers)
        {
            observer.OnNotifyUpdate(CurrentState);
        }
    }

    public void ReturnToPreviousState()
    {
        CurrentState = PreviousState;
    }
}
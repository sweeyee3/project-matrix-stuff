﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObserver
{
    void OnNotifyExit();
    void OnNotifyEnter();
    void OnNotifyUpdate();
    void OnNotifyChange();
}

// generic for optional parameter
public interface IObserver<T>
{
    void OnNotifyExit(T param);
    void OnNotifyEnter(T param);
    void OnNotifyUpdate(T param);
    void OnNotifyChange(T param);
}


public interface ISubject<O>
{
    List<O> Observers { get; set; }
    void AddObserver(O observer);
    void RemoveObserver(O observer);
}


public class TestSubject : ISubject<IObserver>
{
    public List<IObserver> Observers { get; set; }

    public void AddObserver(IObserver observer)
    {
        if (!Observers.Contains(observer))
            Observers.Add(observer);
    }

    public void RemoveObserver(IObserver observer)
    {
        if (Observers.Contains(observer))
            Observers.Remove(observer);
    }
}

public class TestObserver : IObserver
{
    public void OnNotifyChange()
    {
    }

    public void OnNotifyEnter()
    {
    }

    public void OnNotifyExit()
    {
    }

    public void OnNotifyUpdate()
    {
    }
}
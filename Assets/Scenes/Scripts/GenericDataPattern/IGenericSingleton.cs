﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonGeneric<T> where T : class, new()
{
    SingletonGeneric() { }

    class Create
    {
        static Create() { }
        // Private object instantiated with private constructor
        internal static readonly T instance = new T();
    }

    public static T Instance
    {
        get { return Create.instance; }
    }
}

public static class SingletonGameObject<T> where T : MonoBehaviour
{
    private static T _instance;
    private static object m_Lock = new object(); // ensures thread safety

    public static T Instance
    {
        get
        {
            lock (m_Lock)
            {
                if (_instance != null)
                    return _instance;

                var objs = Object.FindObjectsOfType(typeof(T));
                if (objs.Length > 0)
                {
                    if (objs.Length == 1)
                        return _instance = objs[0] as T;

                    Debug.LogWarning(typeof(T).ToString() + ", More than 1 Singleton! First intance used, all others destroyed.");

                    for (var i = 1; i < objs.Length; i++)
                        Object.Destroy(objs[i]);
                    return _instance = objs[0] as T;
                }
                GameObject obj = new GameObject(typeof(T).ToString());
                Object.DontDestroyOnLoad(obj);
                return _instance = obj.AddComponent<T>();
            }
        }
    }
}

public class TestSingletonGameObject : MonoBehaviour
{
    public static TestSingletonGameObject Instance { get { return SingletonGameObject<TestSingletonGameObject>.Instance; } }
    public void Print() { Debug.Log("testing game object singleton"); }
}

public class TestSingletonGeneric
{
    public static TestSingletonGeneric Instance { get { return SingletonGeneric<TestSingletonGeneric>.Instance; } }
    public void Print() { Debug.Log("testing generic singleton"); }
}

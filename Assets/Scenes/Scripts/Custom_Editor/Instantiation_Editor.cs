﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// 
/// CustomEditor type should not override Unity's built-in types:
/// https://answers.unity.com/questions/343984/custom-editor-makes-inspector-view-act-weirdly.html
/// https://forum.unity.com/threads/custom-editor-script-what-is-typeof-and-other-weirdness.530675/
/// 
/// </summary>

namespace Custom_Editor
{
    public class Instantiation_Editor : Editor
    {
        public static GameObject Instantiate(string path)
        {
            var prefab = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));
            if (prefab == null)
            {
                Debug.LogError("prefab cannot be found!");
                return null;
            }
            
            return Instantiate(prefab as GameObject);
        }

        public static GameObject Instantiate(GameObject prefab)
        {
            var spawnedObject = PrefabUtility.InstantiatePrefab(prefab as GameObject) as GameObject;

            if (spawnedObject == null)
            {
                Debug.Log("prefab cannot be instantiated!");
                return null;
            }

            Debug.Log("prefab instantiated!");

            return spawnedObject;
        }

        public static void SetRoot(GameObject obj, string path)
        {
            GameObject root = GameObject.Find(path);
            SetRoot(obj, root);
        }

        public static void SetRoot(GameObject obj, GameObject root)
        {
            obj.transform.SetParent(root.transform);
        }	
    }
}

#endif

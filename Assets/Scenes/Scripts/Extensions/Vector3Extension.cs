﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extension
{
    public static int IndexOfLargestComponent(this Vector3 vector)
    {
        var array = new float[3];
        array[0] = Mathf.Abs(vector[0]);
        array[1] = Mathf.Abs(vector[1]);
        array[2] = Mathf.Abs(vector[2]);
        var largest = array[0];
        var largestIndex = 0;

        for (var i=1; i<array.Length; i++)
        {
            if (array[i] > largest)
            {
                largest = array[i];
                largestIndex = i;
            }
        }
        return largestIndex;
    }

    public static int IndexOfSmallestComponent(this Vector3 vector)
    {
        var array = new float[3];
        array[0] = Mathf.Abs(vector[0]);
        array[1] = Mathf.Abs(vector[1]);
        array[2] = Mathf.Abs(vector[2]);
        var smallest = array[0];
        var smallestIndex = 0;

        for (var i = 1; i < array.Length; i++)
        {
            if (array[i] < smallest)
            {
                smallest = array[i];
                smallestIndex = i;
            }
        }
        return smallestIndex;
    }

    public static int IndexOfMiddleComponent(this Vector3 vector)
    {
        var largestIndex = IndexOfLargestComponent(vector);
        var smallestIndex = IndexOfSmallestComponent(vector);
        var middleIndex = 0;

        if ((largestIndex == 0 && smallestIndex == 1) || (largestIndex == 1 && smallestIndex == 0))
        {
            middleIndex = 2;
        }
        else if ((largestIndex == 0 && smallestIndex == 2) || (largestIndex == 2 && smallestIndex == 0))
        {
            middleIndex = 1;
        }

        return middleIndex;
    }

    public static Vector3 GetDirection(this Vector3 startPosition, Vector3 endPosition)
    {
        return endPosition - startPosition;
    }

    public static bool IsSameDirection(this Vector3 vector, Vector3 otherVector)
    {
        return Vector3.Distance(vector.normalized, otherVector.normalized) <= 0.001f;
    }

    public static Vector3 WorldToUISpace(this Vector3 vector, Canvas parentCanvas)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = parentCanvas.worldCamera.WorldToScreenPoint(vector);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        //Convert the local point to world point
        return parentCanvas.transform.TransformPoint(movePos);
    }
}

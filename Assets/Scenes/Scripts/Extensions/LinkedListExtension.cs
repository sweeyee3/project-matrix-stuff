﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LinkedListExtension
{
    public static LinkedListNode<T> NodeAt<T>(this LinkedList<T> linkedList, int index)
    {
        if (index < 0 || index + 1 > linkedList.Count)
        {
            throw new System.IndexOutOfRangeException("Index");
        }
        int counter = 0;
        var next = linkedList.First;

        while (next != null)
        {
            if (counter == index)
                return next;

            counter++;
            next = next.Next;
        }                
        return null;
    }
}

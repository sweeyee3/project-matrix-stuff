# Toy 3D Matrix Transformation project

## Why do this?
Mostly to help me reinforce some concepts about Linear Transformations in the context of 3D space.

## What to look for in this project
Main things:
- In the Scenes folder, open "start"
- In the hierarchy, open root
- Adjust the "Mesh" script to see the different transformation effects

Main scripts powering this:
- Mesh.cs
- Mesh_Editor.cs

Important classes:
- public class Transformation
  - Wrapper class for Transform manipulation.
  - Mostly using Unity Matrix4x4 as matrix representation
- public class Mesh : Monobehaviour
  - Stores the actual "Mesh", i.e. the points
- public class Mesh_Editor : Editor
  - All the custom editor stuff resides here

## Approach
An important thing to understand with this project is that it's done from the perspective of **Matrices** and **Axis manipulation**\
Therefore, unlike typical 3D engines, it stores everything in Matrix4x4 matrices. (Not the usual Quaternions and Vector3s, which turns out to be more efficient storage wise)\
**So a big disclaimer: this is not meant to replace the standard Unity Transform components.**

However, now that matrices are stored, exposing and manipulating them directly becomes possible.\
And this can lead to some pretty interesting insights.

### Why see things as matrices?
Fun! But more than that, it's more about a change in perspective. The way Unity exposes transformation manipulation are via these 3 components (abbreviated to TRS),
- Translation (as 3 values representing positions, x, y, z)
- Rotation (as 3 values representing the rotation angle about a given axis in degrees, x, y, z)
- Scale (as 3 values representing some scaling factor, x, y, z)

And it makes sense. This representation is a very intuitive way to understand and manipulate objects in 3D. (Which is also why all modern 3D software application uses this!)

But we shouldn't forget that under the hood, a lot of manipulation happens to make these intuitive systems work.\
And one thing that gets hidden from us is this: Matrix Transformations.

And hiding Matrix transformation hides a rather neat idea about manipulating objects in 3D space,\
**That it is about manipulating your axes.**

### Manipulating axes / grids
3blue1brown's series on Linear Algebra explains this way better than I did. But the idea is this:

Every point in space can be described by "combining" unit sized arrows, which we will call "vectors".\
Turns out, in 3D space, we only need 3 of these vectors*.\
So now, if we manipulate these vectors, then every point that these vectors can make, will also have the same kind of manipulation applied to them.

These vectors are what we colloquially term as "axes" (or bases from the mathematical perspective).

So where do these vectors / axes come from?

Notes*:
- These 3 vectors, have to satisfy certain properties. Mainly, they have to be independent of each other. Independence is a concept I don't have the confidence to explain but the
key thing is that not just any 3 vectors can be used. An example of non-independent vectors would be if 2 vectors are just scalar multiples of each other. (eg. (1, 0, 0) and (3, 0, 0))

### From vectors to matrices
To simplify things, we use 3 commonly seen vectors as the axes.\
x: (1, 0, 0)\
y: (0, 1, 0)\
z: (0, 0, 1)

Using these 3 vectors, let us see how we can apply TRS to get a point:
- Translation
  - we can simply add the vectors together (i.e. point (1, 1, 1) = x + y + z )
- Rotation
  - we can also add vectors together! But.. with some trigonometry involved ( i.e. point (1, 0, 0) to point (0, 1, 0) = cos(90deg) * x + sin(90deg) * y )
- Scale
  - we can "make a vector longer". Mathematically, that's multiply the same number to all 3 values in an axis ( i.e. point (3, 0, 0) = 3 * x )

Now we know TRS can be done on the axes separately, can I combine the information of the 3 axis into a single representation?\
Turns out, we can. The derivation of it is a little more than what I can properly explain, but ScratchaPixel has a very good explanation on how to combine them.

The result is then.. a very useful 4x4 matrix.\
(1, 0, 0, 0)\
(0, 1, 0, 0)\
(0, 0, 1, 0)\
(0, 0, 0, 1)

Some interesting things to note:
- The "axis" information is encoded in the first 3x3 section of the matrix.
- The last column is to accomodate for translations!
- The extra row (0, 0, 0, 1) is to account for the effects of translation when doing matrix multiplication.

With this matrix exposed, we can literally "adjust the axes" such that all the points that are created by these 3 axes, gets manipulated the same way!

### Some direct random use cases I can think of
- If anyone has played tactical style games, troops would sometimes line up nicely in a square or triangle or whatever elegantly shaped formations.
Placing these "formations" seem like something Matrices can do. And swapping formation shapes could be interpolating between different formation shapes.
- Grouping AI systems. Storing a group of AI's movement patterns as Matrices could work?

## Short term features
- LERP between 2 Transformations ( possibly quite complex )
- Grab parent's Combined Transformation and apply it as the Reference Transformation.
- Storing multiple transforms ( abstract the concept of Root Transforms and Child Transforms and store multiple copies of them )
- Grab Unity's Transform Component and use that as the Reference Transformation.

## Long term features
- Decomposing a matrix to TRS components ( may not be TRS exactly but there is a technique called SVD that can achieve similar decomposition results )